FROM frolvlad/alpine-miniconda3
# FROM ubuntu:latest
# FROM tiangolo/uwsgi-nginx-flask:python3.6-alpine3.7

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

RUN apt-get update -y
RUN apt-get install -y libgl1-mesa-glx
# RUN apt-get update -y
# RUN apt-get install -y python-pip python-dev build-essential
# RUN apt-get update -y && apt-get install -y python3-pip 

COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["app.py"]
