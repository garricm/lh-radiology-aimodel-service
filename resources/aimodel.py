from flask_apispec import doc, marshal_with
from flask_apispec.views import MethodResource
from flask_restful import Resource
from common.schemas.ModelListSchema import ModelListSchema
from common.util.util import scanDirectory

class aimodel(MethodResource, Resource):
    @doc(summary="Get all Models", description='Returns the list of all available models in the system', tags=['Models'])
    @marshal_with(ModelListSchema)
    def get(self):
        subfolders, files = scanDirectory('model/', [".pkl"])
        data = []
        for file in files:
            file_details = file.split("/")
            data.append({'model': file_details[-3], 'version': file_details[-2]})
            
        return { "status": "Success", "data": data }