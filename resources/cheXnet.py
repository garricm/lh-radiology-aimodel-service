import werkzeug
import os
from flask_apispec import doc, marshal_with, use_kwargs
from flask import current_app as app
from flask_apispec.views import MethodResource
from flask_restful import Resource, reqparse
from marshmallow import Schema, fields
from common.schemas.BoundingBoxSchema import BoundingBoxSchema
from common.schemas.ModelListSchema import ModelListSchema
from model.cheXnet.cheXnet import getcoordinates
from common.util.util import scanDirectory

from datetime import datetime
from uuid import uuid4

parser = reqparse.RequestParser()
parser.add_argument('image', type=werkzeug.datastructures.FileStorage, location='files')
parser.add_argument('model', type=str, location='form')
parser.add_argument('modelVersion', type=str, location='form')


class BoundingBoxRequestSchema(Schema):
    image = fields.Raw(description="DICOM Image")
    model = fields.String(description="Model Name")
    modelVersion = fields.String(description="Model Version")


class BoundingBoxResponseSchema(Schema):
    status = fields.String(default='Success', required=True)
    model = fields.String(required=True)
    data = fields.List(fields.Nested(BoundingBoxSchema))


class cheXnet(MethodResource, Resource):
    @doc(summary="Get Bounding Box Models", description='Returns the list of Bounding Box models with the model versions', tags=['Bounding Box'])
    @marshal_with(ModelListSchema)
    def get(self):
        subfolders, files = scanDirectory('model/cheXnet/', [".pkl"])
        data = []
        for file in files:
            file_details = file.split("/")
            data.append({'model': file_details[-3], 'version': file_details[-2]})

        return { "status": "Success", "data": data }

    @doc(summary="Get Annotation Text and Bounding Boxes for DICOM", description='Returns the Bounding Boxes for the DICOM Image with the Annotation Text and the coordinates', tags=['Bounding Box'])
    @use_kwargs(BoundingBoxRequestSchema)
    @marshal_with(BoundingBoxResponseSchema)
    def post(self):
        args = parser.parse_args()
        image_file = args['image']
        model = "" + args['model']
        modelVersion = args['modelVersion']

        filename = werkzeug.utils.secure_filename(image_file.filename)
        app.logger.debug("Recieved image - " + filename)

        app.logger.debug("Creating temp directory if it doesn't exist")
        os.makedirs("temp", exist_ok=True)

        # Handle multiple POST requests with the same filename
        # at the exact same time
        unique_id = datetime.now().strftime('%Y%m-%d%H-%M%S-') + str(uuid4())
        filename = unique_id + "_" + filename

        file_path = "temp/" + filename
        app.logger.debug("Saving file to disk - " + file_path)
        image_file.save(file_path)
        app.logger.debug("Saved file to disk - " + file_path)

        data = getcoordinates(file_path, modelVersion, model)

        if(data is None):
            status = 'Invalid Model'
        else:
            status = 'Success'

        # Cleanup: Delete Files
        if os.path.exists(file_path):
            os.remove(file_path)

        return {'status': status, 'model': model, 'data': data}
