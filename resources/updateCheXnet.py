import werkzeug
from flask_apispec import doc, marshal_with, use_kwargs
from flask import current_app as app
from flask_apispec.views import MethodResource
from flask_restful import Resource, reqparse
from marshmallow import Schema, fields

parser = reqparse.RequestParser()
parser.add_argument('image', type=werkzeug.datastructures.FileStorage, required=True, location='files')
parser.add_argument('x1', type=int, required=True, help="Coordinate x1 cannot be blank!")
parser.add_argument('y1', type=int, required=True, help="Coordinate y1 cannot be blank!")
parser.add_argument('x2', type=int, required=True, help="Coordinate x2 cannot be blank!")
parser.add_argument('y2', type=int, required=True, help="Coordinate y2 cannot be blank!")
parser.add_argument('x3', type=int, required=True, help="Coordinate x3 cannot be blank!")
parser.add_argument('y3', type=int, required=True, help="Coordinate y3 cannot be blank!")
parser.add_argument('x4', type=int, required=True, help="Coordinate x4 cannot be blank!")
parser.add_argument('y4', type=int, required=True, help="Coordinate y4 cannot be blank!")
parser.add_argument('model', type=str, required=True, help="Model Name cannot be blank!")
parser.add_argument('modelVersion', type=int, required=True, help="Model Version Number cannot be blank!")
parser.add_argument('annotationText', type=str, required=True, help="Annotation Text cannot be blank!")

class ModelUpdateRequestSchema(Schema):
    image = fields.Raw(description="DICOM Image")
    x1 = fields.Number(description="x1 co-ordinate of Bounding Box")
    y1 = fields.Number(description="y1 co-ordinate of Bounding Box")
    x2 = fields.Number(description="x2 co-ordinate of Bounding Box")
    y2 = fields.Number(description="y2 co-ordinate of Bounding Box")
    x3 = fields.Number(description="x3 co-ordinate of Bounding Box")
    y3 = fields.Number(description="y3 co-ordinate of Bounding Box")
    x4 = fields.Number(description="x4 co-ordinate of Bounding Box")
    y4 = fields.Number(description="y4 co-ordinate of Bounding Box")
    model = fields.String()
    modelVersion = fields.Number(description="Model Version Number")
    annotationText = fields.String()


class ModelUpdateResponseSchema(Schema):
    status = fields.String(default='Success', required=True)
    model = fields.String(required=True)
    modelVersion = fields.String(required=True)


class updateCheXnet(MethodResource, Resource):
    @doc(summary="Update Bounding Box model", description='Updates the Bounding Box model based on the image and co-ordinates', tags=['Update Model'])
    @use_kwargs(ModelUpdateRequestSchema)
    @marshal_with(ModelUpdateResponseSchema)
    def post(self):
        return {"status": "Success", "data": [{'model': 'chexNet', 'version': '1.0'}]}
