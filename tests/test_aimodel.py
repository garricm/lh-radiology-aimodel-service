import pytest
import json

def test_get(test_client) :
    response = test_client.get("/models")
    
    data = response.json
    print(json.dumps(data, indent = 3))
    
    assert response.status_code == 200
    assert data['status'] == 'Success'